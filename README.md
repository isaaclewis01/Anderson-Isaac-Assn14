# Cuboid Evaluator/Random Number Statistics/Shape Drawer

## NOTE
This was an assignment done for CS1400 at Utah State University, some code from the project was done by Chad Mano as a basic for the assignment, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code.

## Manual
There are three parts to this program:
1. Cuboid Evaluator (run from file 14-task1): This simulates two cubes, defined by the user, and evaluates surface area, volume, which cube is bigger, and this data if they were added and subtracted to each other. 
2. Random Number Statistics (run from file 14-task2): This generates a list of 1000 random integers from 0 to 9, and outputs how many of each one occurs.
3. Shape Drawer (run from the file 14-task3): This program draws shapes onto a canvas, and the user is able to specify a circle or rectangle they would like to have drawn, and its dimentions. Then the user can draw more or remove what has been drawn.
All parts when run have clear instructions on how the user should use each program.
