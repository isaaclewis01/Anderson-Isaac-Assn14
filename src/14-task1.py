# Import statements
from cuboid import Cuboid

def main():
    # User input for cuboids
    c1Side1, c1Side2, c1Side3 = eval(input("Enter the side lengths of the first cuboid (side1, side2, side3): "))
    c2Side1, c2Side2, c2Side3 = eval(input("Enter the side lengths of the second cuboid (side1, side2, side3): "))

    # Define cuboid one and two
    cuboid1 = Cuboid(c1Side1, c1Side2, c1Side3)
    cuboid2 = Cuboid(c2Side1, c2Side2, c2Side3)

    # Addition and subtraction of cuboids
    cuboid3 = cuboid1 + cuboid2
    cuboid4 = cuboid1 - cuboid2

    # Print cuboids (side lengths, volume, surface area)
    print("Cuboid one is greater than cuboid two: " + str(cuboid1 > cuboid2))
    print("Cuboid one is less than cuboid two: " + str(cuboid1 < cuboid2))
    print("Cuboid one is equal to cuboid two: " + str(cuboid1 == cuboid2))
    print("Cuboid 1:\n" + str(cuboid1))
    print("Cuboid 2:\n" + str(cuboid2))
    print("Cuboid 3 (cuboid one plus cuboid two):\n" + str(cuboid3))
    print("Cuboid 4 (absolute value of cuboid one minus cuboid two):\n" + str(cuboid4))

main()