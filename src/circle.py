# Class to define and draw circle
class Circle():
    def __init__(self, tr, posX, posY, radius, color):
        self.__tr = tr
        self.__posX = posX
        self.__posY = posY
        self.__radius = radius
        self.__color = color

    def draw(self):
        self.__tr.penup()
        self.__tr.color(self.__color)
        self.__tr.goto(self.__posX, self.__posY - self.__radius)
        self.__tr.begin_fill()
        self.__tr.pendown()
        self.__tr.circle(self.__radius)
        self.__tr.end_fill()