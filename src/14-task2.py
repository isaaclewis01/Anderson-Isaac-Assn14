# Import statements
import random

def main():
    # Define the list
    list = []
    # Loop to create list of random numbers
    for i in range(1000):
        list.append(random.randint(0, 9))
    # Print the count of each number
    print("A list of 1000 random integers from 0 to 9 is generated, this is how many of each number occurs: ")
    for i in range(10):
        print("\t" + str(i) + "s: " + str(list.count(i)))

main()