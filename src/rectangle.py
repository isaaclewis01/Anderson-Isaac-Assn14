# Create rectangle class to define and draw rectangle
class Rectangle():
    def __init__(self, tr, posX, posY, height, width, color):
        self.__tr = tr
        self.__posX = posX
        self.__posY = posY
        self.__height = height
        self.__width = width
        self.__color = color

    def draw(self):
        self.__tr.penup()
        self.__tr.color(self.__color)
        self.__tr.goto(self.__posX, self.__posY)
        self.__tr.begin_fill()
        self.__tr.pendown()
        for i in range(2):
            self.__tr.forward(self.__width)
            self.__tr.left(90)
            self.__tr.forward(self.__height)
            self.__tr.left(90)
        self.__tr.end_fill()