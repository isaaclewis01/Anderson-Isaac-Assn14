# import statements
from circle import Circle
import turtle
from rectangle import Rectangle

def main():
    # Define list
    shapeList = []

    # Define turtle
    tr = turtle.Turtle()

    # Loop to continue playing
    play = True
    while play:

        # Input menu
        print("\t1) Enter circle")
        print("\t2) Enter rectangle")
        print("\t3) Remove shape")
        print("\t4) Draw shapes")
        print("\t0) Exit")
        choice = eval(input("Choose an option from the menu (0-4): "))

        # Choice one
        if choice == 1:
            # User input for circle
            print("For circle, enter values for: ")
            circPosX, circPosY = eval(input("\tX coordinate, Y coordinate: "))
            radius = eval(input("\tRadius: "))
            color = input("\tFill color (red, yellow, blue, or green): ")
            if color.lower() == "red" or color.lower() == "green" or color.lower() == "blue" or color.lower() == "yellow":
                shapeList.append(Circle(tr, circPosX, circPosY, radius, color))
            else:
                print("Not a valid color.")

        elif choice == 2:
            # User input for rectangle
            print("For rectangle, enter values for: ")
            rectPosX, rectPosY = eval(input("\tX coordinate, Y coordinate: "))
            height = eval(input("\tHeight: "))
            width = eval(input("\tWidth: "))
            color = input("\tFill color (red, yellow, blue, or green): ")
            if color.lower() == "red" or color.lower() == "green" or color.lower() == "blue" or color.lower() == "yellow":
                shapeList.append(Rectangle(tr, rectPosX, rectPosY, height, width, color))
            else:
                print("Not a valid color.")

        elif choice == 3:
            # Remove shape
            if len(shapeList) > 0:
                for i in range(len(shapeList)):
                    print(str(i) + ") " + str(shapeList[i]))
                remove = eval(input("Choose which shape to remove: "))
                shapeList.remove(shapeList[i - 1])
            else:
                print("No shapes to remove.")

        elif choice == 4:
            # Draw shapes
            tr.clear()
            for i in range(len(shapeList)):
                shapeList[i].draw()
            tr.hideturtle()
        else:
            # Exit loop
            play = False

    turtle.done()
main()