# Define cuboid class
class Cuboid:
    def __init__(self, side1, side2, side3):
        self.__side1 = side1
        self.__side2 = side2
        self.__side3 = side3

    # Overload string operator
    def __str__(self):
        return ("\tSide lengths: " + str(self.__side1) + ", " + str(self.__side2) + ", " + str(self.__side3) + "\n" +
                "\tVolume: " + str(self.volume()) + "\n" +
                "\tSurface area: " + str(len(self)))

    # Overload length operator
    def __len__(self):
        return int(2 * (self.__side1 * self.__side2) + 2 * (self.__side2 * self.__side3) + 2 * (self.__side1 * self.__side3))

    # Find volume of cuboid
    def volume(self):
        return round(self.__side1 * self.__side2 * self.__side3)

    # Overload greater than operator
    def __gt__(self, other):
        return True if self.volume() > other.volume() else False

    # Overload less than operator
    def __lt__(self, other):
        return True if self.volume() < other.volume() else False

    # Overload equal operator
    def __eq__(self, other):
        return True if self.volume() == other.volume() else False

    # Overload the addition operator
    def __add__(self, other):
        side = (self.volume() + other.volume()) ** (1/3)
        return Cuboid(side, side, side)

    # Overload the subtraction operator
    def __sub__(self, other):
        side = abs((self.volume() - other.volume()) ** (1 / 3))
        return Cuboid(side, side, side)